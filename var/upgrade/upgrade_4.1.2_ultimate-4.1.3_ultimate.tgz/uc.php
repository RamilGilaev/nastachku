<?php
use \Tygh\Less;
use Tygh\Registry;

if ($upgrade_type == 'upgrade') {
    fn_uc_restore_settings();
    $searchanise_status = db_get_field("SELECT status FROM ?:addons WHERE addon = 'searchanise'");
    if (!empty($searchanise_status) && $searchanise_status != 'D') {
        db_query("UPDATE ?:addons SET status = 'D' WHERE addon = 'searchanise'");
        fn_set_notification('W', __('warning'), __('uc_searchanise_disabled', array('[url]' => fn_url('addons.manage'))));
    }
    $config_content = file_get_contents(Registry::get('config.dir.root') . '/config.local.php');
    if (!empty($config_content)) {
        if (!strpos($config_content, "Log everything, but do not display")) {
            $config_content = str_replace("Disable notices displaying\nerror_reporting(E_ALL ^ E_NOTICE);", "Log everything, but do not display", $config_content);
            $config_content = str_replace("if (version_compare(PHP_VERSION, '5.3.0', '>=')) {\n    error_reporting(error_reporting() & ~E_DEPRECATED & ~E_STRICT);\n}", "error_reporting(E_ALL);\nini_set('display_errors', 0);", $config_content);
            $config_content = str_replace(" // set to true to disable js files compilation\n", ", // set to true to disable js files compilation\n    'redirect_to_cart' => false // Redirect customer to the cart contents page. Used with the \"disable_dhtml\" setting.\n", $config_content);
            fn_put_contents(Registry::get('config.dir.root') . '/config.local.php', $config_content);
        }
    }
    $htaccess_content = file_get_contents(Registry::get('config.dir.root') . '/.htaccess');
    if (!empty($htaccess_content)) {
        if (!strpos($htaccess_content, 'Header unset ETag')) {
            $htaccess_content = str_replace("<FilesMatch .*\.css.gz$>", "<FilesMatch .*\.css.gz$>\n        <IfModule mod_expires.c>\n            Header unset ETag\n            FileETag None\n            ExpiresActive On\n            ExpiresDefault \"access plus 1 year\"\n        </IfModule>\n", $htaccess_content);
            $htaccess_content = str_replace("<FilesMatch .*\.js.gz$>", "<FilesMatch .*\.js.gz$>\n        <IfModule mod_expires.c>\n            Header unset ETag\n            FileETag None\n            ExpiresActive On\n            ExpiresDefault \"access plus 1 year\"\n        </IfModule>\n", $htaccess_content);
            $htaccess_content = str_replace("RewriteRule api", "RewriteRule ^api", $htaccess_content);
            fn_put_contents(Registry::get('config.dir.root') . '/.htaccess', $htaccess_content);
        }
    }

    $themes_paths = array(Registry::get('config.dir.design_frontend'), Registry::get('config.dir.themes_repository'));
    foreach ($themes_paths as $themes_path) {
        $themes = scandir($themes_path);
        if (!empty($themes)) {
            foreach ($themes as $theme) {
                if (is_dir($themes_path . '/' . $theme)) {
                    if (!file_exists($themes_path . '/' . $theme . '/manifest.json') && file_exists($themes_path . '/' . $theme . '/manifest.ini')) {
                        convertIni2Json($themes_path . '/' . $theme . '/manifest.ini', $themes_path . '/' . $theme . '/manifest.json');
                    }
                }
            }
        }
    }

	// Cleanup templates/cache
	fn_clear_cache();

} else {
	fn_clear_cache();
}

function convertIni2Json($from, $to)
{
    if (file_exists($from)) {
        $manifest = parse_ini_file($from);
        if (file_put_contents($to, json_encode($manifest)) === false) {
            echo 'Unable to write ' . $to . '. Check folder permissions.<br>';
        }
    } else {
        echo 'Failed to convert ' . $from . '. File not found<br>';
    }
}
?>
