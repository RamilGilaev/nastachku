DROP TABLE IF EXISTS cscart_settings_vendor_values_upg;
CREATE TABLE `cscart_settings_vendor_values_upg` (
  `object_id` mediumint(8) unsigned NOT NULL auto_increment,
  `company_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL default '',
  `section_name` varchar(128) NOT NULL default '',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`object_id`, `company_id`)
) Engine=MyISAM DEFAULT CHARSET UTF8;

INSERT INTO cscart_settings_vendor_values_upg
    SELECT
        cscart_settings_objects.object_id,
        company_id,
        cscart_settings_objects.name,
        cscart_settings_sections.name as section_name,
        cscart_settings_vendor_values.value
    FROM cscart_settings_objects
    LEFT JOIN cscart_settings_sections ON cscart_settings_sections.section_id = cscart_settings_objects.section_id
    INNER JOIN cscart_settings_vendor_values ON cscart_settings_vendor_values.object_id = cscart_settings_objects.object_id;

DELETE FROM cscart_settings_vendor_values WHERE object_id IN (
    SELECT object_id FROM cscart_settings_objects WHERE section_id IN (
        SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
    )
);

DROP TABLE IF EXISTS cscart_settings_objects_upg;
CREATE TABLE `cscart_settings_objects_upg` (
  `object_id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(128) NOT NULL default '',
  `section_name` varchar(128) NOT NULL default '',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`object_id`)
) Engine=MyISAM DEFAULT CHARSET UTF8;

INSERT INTO cscart_settings_objects_upg
    SELECT
        cscart_settings_objects.object_id,
        cscart_settings_objects.name,
        cscart_settings_sections.name as section_name,
        cscart_settings_objects.value
    FROM cscart_settings_objects
    LEFT JOIN cscart_settings_sections ON cscart_settings_sections.section_id = cscart_settings_objects.section_id
    WHERE cscart_settings_objects.section_id IN (SELECT cscart_settings_sections.section_id FROM cscart_settings_sections WHERE cscart_settings_sections.type = 'ADDON');

DELETE FROM cscart_settings_descriptions WHERE object_type = 'V' AND object_id IN (
    SELECT variant_id FROM cscart_settings_variants WHERE object_id IN (
        SELECT object_id FROM cscart_settings_objects WHERE section_id IN (
            SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
        )
    )
);

DELETE FROM cscart_settings_descriptions WHERE object_type = 'O' AND object_id IN (
    SELECT object_id FROM cscart_settings_objects WHERE section_id IN (
        SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
    )
);

DELETE FROM cscart_settings_descriptions WHERE object_type = 'S' AND object_id IN (
    SELECT section_id FROM cscart_settings_sections WHERE parent_id IN (
        SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
    )
);

DELETE FROM cscart_settings_descriptions WHERE object_type = 'S' AND object_id IN (
    SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
);

DELETE FROM cscart_settings_variants WHERE object_id IN (
    SELECT object_id FROM cscart_settings_objects WHERE section_id IN (
        SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
    )
);

DELETE FROM cscart_settings_objects WHERE section_id IN (
    SELECT section_id FROM cscart_settings_sections WHERE type = 'ADDON'
);

DELETE s1, s2 FROM cscart_settings_sections s1 LEFT JOIN cscart_settings_sections as s2 ON s2.parent_id = s1.section_id WHERE s1.type = 'ADDON';

INSERT INTO `cscart_settings_objects` (object_id, edition_type, name, section_id, section_tab_id, type, value, position, is_global, handler) VALUES ('57', 'ROOT,ULT:VENDOR', 'keep_https', '2', '0', 'C', 'N', '5', 'Y', '') ON DUPLICATE KEY UPDATE `object_id` = `object_id`;

DELETE FROM `cscart_payment_descriptions` WHERE payment_id IN (SELECT payment_id FROM ?:payments WHERE processor_id IN (SELECT processor_id FROM `?:payment_processors` WHERE processor_script = 'google_checkout.php'));
DELETE FROM `cscart_payments` WHERE processor_id IN (SELECT processor_id FROM `?:payment_processors` WHERE processor_script = 'google_checkout.php');
DELETE FROM `cscart_payment_processors` WHERE processor_script='google_checkout.php';
DELETE FROM `cscart_settings_objects` WHERE name='redirect_to_cart';
