<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

// Do not delete comments below

//[params]
$uc_settings = array (
  'license_number' => '',
  'ftp_hostname' => '',
  'ftp_username' => '',
  'ftp_password' => '',
  'ftp_directory' => '',
);

$db = array ('db_host' => 'localhost','db_user' => 'devel','db_password' => 'UtW3CEDc896rnKj3','db_name' => 'devel_stachka');

$multistage = false;

$dir_cache_templates = '/var/www/html/shop.nastachku.ru/var/cache/templates/';

$dir_cache_registry = '/var/www/html/shop.nastachku.ru/var/cache/registry/';

$dir_cache_misc = '/var/www/html/shop.nastachku.ru/var/cache/misc/';

$uak = 'f0f96a0c7bb95ffa63d708883e70eb9a';
//[/params]

if (empty($_GET['uak']) || $_GET['uak'] != $uak) {
    die('Access denied');
}

set_time_limit(0);
if (stristr(PHP_OS, 'WIN')) {// Define operation system
    define('IS_WINDOWS', true);
}

$path = explode('/', fn_unified_path(dirname(__FILE__)));
$package = array_pop($path);

define('DIR_UPGRADE', implode('/', $path) . '/');
array_pop($path);
array_pop($path);
if (!empty($multistage)) {
    array_pop($path);
}
define('DIR_ROOT', implode('/', $path));
define('PACKAGE', $package);
define('DEFAULT_FILE_PERMISSIONS', 0644);
define('DEFAULT_DIR_PERMISSIONS', 0755);
define('DIR_THEMES', DIR_ROOT . '/design');
define('AREA', 'A');

if (!empty($uc_settings['ftp_hostname'])) {
    fn_echo(fn_tr('text_connecting_to_ftp'));
    if ($r = fn_uc_ftp_connect($uc_settings) === true) {
        fn_echo(fn_tr('ok'));
    } else {
        fn_echo(fn_tr($r));
    }
}

fn_echo(fn_tr('text_copying_files'));
fn_uc_copy_files(DIR_UPGRADE . PACKAGE . '/backup', DIR_ROOT);

fn_echo(fn_tr('text_removing_obsolete_files'));
fn_uc_rm_files(DIR_ROOT, DIR_UPGRADE . PACKAGE . '/uc.xml', 'new_files');

@fn_uc_rm(DIR_ROOT . '/uc.sql');

fn_echo(fn_tr('text_updating_database'));
db_connect($db['db_host'], $db['db_user'], $db['db_password'], $db['db_name']);
fn_uc_upgrade_database(DIR_UPGRADE . PACKAGE . '/backup', false);
//fn_uc_post_upgrade(DIR_UPGRADE . $_REQUEST['package'], 'revert'); ?? post upgrade depend on init scripts

if (file_exists(DIR_UPGRADE . 'installed_upgrades.php')) {
    include(DIR_UPGRADE . 'installed_upgrades.php');

    if (isset($udata[PACKAGE])) {
        unset($udata[PACKAGE]);
    }

    if (!empty($udata)) {
        fn_uc_update_installed_upgrades($udata);
    } else {
        fn_rm(DIR_UPGRADE . 'installed_upgrades.php');
    }
}

// remove the lock
$lock_file_path = DIR_UPGRADE . '.upgrade_lock';
if (is_file($lock_file_path)) {
    @unlink($lock_file_path);
    if (is_file($lock_file_path)) {
        fn_echo(str_replace('[file]', $lock_file_path, fn_tr('text_uc_unable_to_remove_upgrade_lock')));
    }
}

fn_rm(DIR_UPGRADE . 'packages.xml'); // cleanup packages list
fn_uc_cleanup_cache(PACKAGE, 'revert');

if (!empty($dir_cache_templates)) {
    fn_rm($dir_cache_templates, false);
}
if (!empty($dir_cache_registry)) {
    fn_rm($dir_cache_registry, false);
}
if (!empty($dir_cache_misc)) {
    fn_rm($dir_cache_misc, false);
}

fn_echo(fn_tr('done'));

/**
 * Check if file is writable
 *
 * @param string $path file path
 * @param boolean $extended return extended status
 * @return boolean true if file is writable, false - otherwise
 */
function fn_uc_is_writable($path, $extended = false)
{
    $result = false;
    $extended_result = array(
        'result' => false,
        'no_ftp' => false,
        'method' => ''
    );

    // File does not exist, check if directory is writable
    if (!file_exists($path)) {
        $a = explode('/', $path);
        do {
            array_pop($a);
        } while (!is_dir(implode('/', $a)));

        $path = implode('/', $a);
    }

    // Check if file can be written using php
    if (!fn_uc_is_writable_dest($path)) {
        $result = fn_uc_ftp_is_writable($path);
        if ($result == false) {
            $ftp = Registry::get('uc_ftp');
            if (!is_resource($ftp)) {
                $extended_result['no_ftp'] = true;
            }
        } else {
            $extended_result['method'] = 'ftp';
        }
    } else {
        $result = true;
        $extended_result['method'] = 'fs';
    }

    $extended_result['result'] = $result;

    return ($extended) ? $extended_result : $result;
}

/**
 * Create directory taking into account accessibility via php/ftp
 *
 * @param string $dir directory
 * @return boolean true if directory created successfully, false - otherwise
 */
function fn_uc_mkdir($dir)
{
    // Try to make directory using php
    $r = fn_uc_is_writable($dir, true);

    $result = $r['result'];
    if ($r['method'] == 'fs') {
        $result = fn_mkdir($dir);
    } elseif ($r['method'] == 'ftp') {
        $result = fn_uc_ftp_mkdir($dir);
    }

    return $result;
}

/**
 * Copy file taking into account accessibility via php/ftp
 *
 * @param string $source source file
 * @param string $dest destination file/directory
 * @return boolean true if directory copied correctly, false - otherwise
 */
function fn_uc_copy($source, $dest)
{
    $result = false;
    $file_name = basename($source);

    if (!file_exists($dest)) {
        if (basename($dest) == $file_name) { // if we're copying the file, create parent directory
            fn_uc_mkdir(dirname($dest));
        } else {
            fn_uc_mkdir($dest);
        }
    }

    fn_echo(' .');

    if (fn_uc_is_writable_dest($dest) || (fn_uc_is_writable_dest(dirname($dest)) && !file_exists($dest))) {
        if (is_dir($dest)) {
            $dest .= '/' . basename($source);
        }
        $result = copy($source, $dest);
        fn_uc_chmod_file($dest);
    }

    if (!$result) { // try ftp
        $result = fn_uc_ftp_copy($source, $dest);
    }

    return $result;
}

function fn_uc_chmod_file($filename)
{
    $ext = fn_get_file_ext($filename);
    $perm = ($ext == 'php' ? 0644 : DEFAULT_FILE_PERMISSIONS);

    $result = @chmod($filename, $perm);

    if (!$result) {
        $ftp = Registry::get('uc_ftp');
        if (is_resource($ftp)) {
            $dest = dirname($filename);
            $dest = rtrim($dest, '/') . '/'; // force adding trailing slash to path

            $rel_path = str_replace(DIR_ROOT . '/', '', $dest);
            $cdir = ftp_pwd($ftp);

            if (empty($rel_path)) { // if rel_path is empty, assume it's root directory
                $rel_path = $cdir;
            }

            if (ftp_chdir($ftp, $rel_path)) {
                $result = @ftp_site($ftp, "CHMOD " . sprintf('0%o', $perm) . " " . basename($filename));
                ftp_chdir($ftp, $cdir);
            }
        }
    }

    return $result;
}

/**
 * Check if destination is writable
 *
 * @param string $dest destination file/directory
 * @return boolean true if writable, false - if not
 */
function fn_uc_is_writable_dest($dest)
{
    $dest = rtrim($dest, '/');

    if (is_file($dest)) {
        $f = @fopen($dest, 'ab');
        if ($f === false) {
            return false;
        }
        fclose($f);
    } elseif (is_dir($dest)) {
        if (!fn_put_contents($dest . '/zzzz.zz', '1')) {
            return false;
        }
        fn_rm($dest . '/zzzz.zz');
    } else {
        return false;
    }

    return true;
}

/**
 * Copy files from one directory to another
 *
 * @param string $source source directory
 * @param string $dest destination directory
 * @return boolean true if directory copied correctly, false - otherwise
 */
function fn_uc_copy_files($source, $dest)
{
    // Simple copy for a file
    if (is_file($source)) {
        return fn_uc_copy($source, $dest);
    }

    // Loop through the folder
    if (is_dir($source)) {
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            if ($dest !== $source . '/' . $entry) {
                if (fn_uc_copy_files(rtrim($source, '/') . '/' . $entry, $dest . '/' . $entry) == false) {
                    return false;
                }
            }
        }

        // Clean up
        $dir->close();

        return true;
    } else {
        return false;
    }
}

/**
 * Upgrade database
 *
 * @param string $path directory with database file
 * @param bool $track track executed queries yes/no
 * @return boolean always true
 */
function fn_uc_upgrade_database($path, $track)
{
    $executed_queries = array();
    if (file_exists($path . '/uc.sql.tmp') && $track == true) {
        $executed_queries = unserialize(fn_get_contents($path . '/uc.sql.tmp'));
    }

    if (file_exists($path . '/uc.sql')) {
        $f = fopen($path . '/uc.sql', 'r');
        if ($f) {
            $ret = array();
            $rest = '';
            while (!feof($f)) {
                $str = $rest . fread($f, 1024);
                $rest = fn_parse_queries($ret, $str);

                if (!empty($ret)) {
                    foreach ($ret as $query) {
                        if (!in_array($query, $executed_queries)) {
                            fn_echo(' .');
                            db_query($query); // FIXME: how to use table prefixes?
                            if ($track == true) {
                                $executed_queries[] = $query;
                                fn_put_contents($path . '/uc.sql.tmp', serialize($executed_queries));
                            }
                        }
                    }

                    $ret = array();
                }
            }

            fclose($f);
        }
    }

    return true;
}

/**
 * Run post-upgrade script
 *
 * @param string $path directory with post-upgrade script
 * @param string $upgrade_type script execution type - "upgrade" or "revert"
 * @return boolean always true
 */
function fn_uc_post_upgrade($path, $upgrade_type)
{
    if (file_exists($path . '/uc.php')) {
        include($path . '/uc.php');
    }

    return true;
}

/**
 * Create directory structure for upgrade
 *
 * @return boolean true if structured created correctly, false - otherwise
 */
function fn_uc_create_structure()
{
    return fn_mkdir(DIR_UPGRADE);
}

/**
 * Check if file is writable using ftp
 *
 * @param string $path file path
 * @return boolean true if file is writable, false - otherwise
 */
function fn_uc_ftp_is_writable($path)
{
    $result = false;
    // If ftp connection is available, check file/directory via ftp
    $ftp = Registry::get('uc_ftp');
    if (is_resource($ftp)) {
        $rel_path = ltrim(str_replace(DIR_ROOT, '', $path), '/');
        if (empty($rel_path)) {
            $rel_path = '.';
        }
        $ftp_path = (is_dir($path) || is_file($path)) ?  $rel_path : (dirname($rel_path));
        if (is_file($path)) {
            $perm = (fn_get_file_ext($path) == 'php') ? 0644 : DEFAULT_FILE_PERMISSIONS;
        } else {
            $perm = DEFAULT_DIR_PERMISSIONS;
        }
        if (ftp_site($ftp, 'CHMOD ' . sprintf('0%o', $perm) . ' ' . $ftp_path)) {
            $result = true;
        }
    }

    return $result;

}

/**
 * Copy file using ftp
 *
 * @param string $source source file
 * @param string $dest destination file/directory
 * @return boolean true if copied successfully, false - otherwise
 */
function fn_uc_ftp_copy($source, $dest)
{
    $result = false;

    $ftp = Registry::get('uc_ftp');
    if (is_resource($ftp)) {
        if (!is_dir($dest)) { // file
            $dest = dirname($dest);
        }
        $dest = rtrim($dest, '/') . '/'; // force adding trailing slash to path

        $rel_path = str_replace(DIR_ROOT . '/', '', $dest);
        $cdir = ftp_pwd($ftp);

        if (empty($rel_path)) { // if rel_path is empty, assume it's root directory
            $rel_path = $cdir;
        }

        if (ftp_chdir($ftp, $rel_path) && ftp_put($ftp, basename($source), $source, FTP_BINARY)) {
            $ext = fn_get_file_ext($source);
            @ftp_site($ftp, "CHMOD " . (fn_get_file_ext($source) == 'php' ? '0644' : sprintf('0%o', DEFAULT_FILE_PERMISSIONS)) . " " . basename($source));
            $result = true;
            ftp_chdir($ftp, $cdir);
        }
    }

    return $result;
}

/**
 * Create directory using ftp
 *
 * @param string $dir directory
 * @return boolean true if directory created successfully, false - otherwise
 */
function fn_uc_ftp_mkdir($dir)
{
    if (@is_dir($dir)) {
        return true;
    }

    $ftp = Registry::get('uc_ftp');
    if (!is_resource($ftp)) {
        return false;
    }

    $result = false;

    $rel_path = str_replace(DIR_ROOT . '/', '', $dir);
    $path = '';
    $dir_arr = array();
    if (strstr($rel_path, '/')) {
        $dir_arr = explode('/', $rel_path);
    } else {
        $dir_arr[] = $rel_path;
    }

    foreach ($dir_arr as $k => $v) {
        $path .= (empty($k) ? '' : '/') . $v;
        if (!@is_dir(DIR_ROOT . '/' . $path)) {
            if (ftp_mkdir($ftp, $path)) {
                $result = true;
            } else {
                $result = false;
                break;
            }
        } else {
            $result = true;
        }
    }

    return $result;
}

/**
 * Connect to ftp server
 *
 * @param array $uc_settings upgrade center options
 * @return boolean true if connected successfully and working directory is correct, false - otherwise
 */
function fn_uc_ftp_connect($uc_settings)
{
    $result = true;

    if (function_exists('ftp_connect')) {
        if (!empty($uc_settings['ftp_hostname'])) {
            $ftp = ftp_connect($uc_settings['ftp_hostname']);
            if (!empty($ftp)) {
                if (@ftp_login($ftp, $uc_settings['ftp_username'], $uc_settings['ftp_password'])) {
                    if (!empty($uc_settings['ftp_directory'])) {
                        ftp_chdir($ftp, $uc_settings['ftp_directory']);
                    }

                    $files = ftp_nlist($ftp, '.');
                    if (!empty($files) && in_array('config.php', $files)) {
                        Registry::set('uc_ftp', $ftp);
                    } else {
                        $result = fn_tr('text_uc_ftp_cart_directory_not_found');
                    }
                } else {
                    $result = fn_tr('text_uc_ftp_login_failed');
                }
            } else {
                $result = fn_tr('text_uc_ftp_connect_failed');
            }
        }
    } else {
        $result = fn_tr('text_uc_no_ftp_module');
    }

    return $result;
}

/**
 * Backup files
 *
 * @param string $source upgrade package directory
 * @param string $dest working directory
 * @param array $result resulting list of backed up files
 * @param string $package package to make backup for
 * @return boolean true if directory copied correctly, false - otherwise
 */
function fn_uc_backup_files($source, $dest, &$result, $package)
{
    // Simple copy for a file
    if (is_file($source)) {
        return fn_uc_backup_file($source, $dest, $result, $package);
    }

    // Loop through the folder
    if (is_dir($source)) {
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep backup directories
            if ($dest !== $source . '/' . $entry) {
                if (fn_uc_backup_files(rtrim($source, '/') . '/' . $entry, $dest . '/' . $entry, $result, $package) == false) {
                    return false;
                }
            }
        }

        // Clean up
        $dir->close();

        return true;
    } else {
        return false;
    }
}

/**
 * Backup certain file
 *
 * @param string $source source file
 * @param string $dest destination file/directory
 * @param array $result resulting list of backed up files
 * @param string $package package to make backup for
 * @return string filename of backed up file
 */
function fn_uc_backup_file($source, $dest, &$result, $package)
{
    $file_name = basename($source);

    if (is_file($dest)) {
        fn_echo(' .');
        $relative_path = str_replace(DIR_ROOT . '/', '', $dest);
        fn_mkdir(dirname(DIR_UPGRADE . $package . '/backup/' . $relative_path));
        fn_copy($dest, DIR_UPGRADE . $package . '/backup/' . $relative_path);
        $result[] = $relative_path;
    }

    return true;
}

/**
 * Function backup obsolete files before deleting
 *
 * @param sting $dest Destanation directory
 * @param string $source Source directory
 * @param string $xml_file Path to xml file with list of files.
 */
function fn_uc_backup_obsolete_files($dest, $source, $xml_file)
{
    $files_list = fn_uc_get_files_from_xml($xml_file, 'deleted_files');

    $themes_files = fn_uc_find_in_themes($files_list, $source);
    $files_list = array_merge($files_list, $themes_files);

    foreach ($files_list as $l) {
        fn_echo(' .');
        fn_mkdir(dirname($dest . $l));
        fn_copy($source . '/' . $l, $dest . $l);
    }

    return $files_list;
}

/**
 * Function remove obsolete or new files after upgrade or reverting upgrade.
 *
 * @param string $source Source directory
 * @param string $xml_file Path to xml file with list of files.
 * @param string $section section of the xml file
 */
function fn_uc_rm_files($source, $xml_file, $section)
{
    $files_list = fn_uc_get_files_from_xml($xml_file, $section);

    $themes_files = fn_uc_find_in_themes($files_list, $source);
    $files_list = array_merge($files_list, $themes_files);

    foreach ($files_list as $file) {
        fn_uc_rm($source . '/' . $file);
    }
}

/**
 * Function finds files from base theme in all installed themes and return full path to those files.
 *
 * @param array $files Array with relative name of the files
 * @param string $source Path to root folder
 * @return array Array of the copies of file from all installed themes.
 */
function fn_uc_find_in_themes($files, $source)
{
    $base_theme = 'var/themes_repository/basic/';
    $len = strlen($base_theme);
    $installed_themes = fn_get_dir_contents(DIR_THEMES);

    $result = array();
    foreach ($files as $file) {
        if (substr($file, 0, $len) == $base_theme) {
            foreach ($installed_themes as $theme_name) {
                $relative_name = "themes/$theme_name/" . substr($file, $len);
                if (file_exists($source . '/' . $relative_name)) {
                    $result[] = $relative_name;
                }
            }
        }
    }

    return $result;
}

/**
 * Function get list of files from package xml. This list will be used for deleting obsolete or new files after upgrading or reverting.
 *
 * @param string $xml_file Path to the xml file
 * @param string $section section of the xml file
 *
 * @return array list of files
 */
function fn_uc_get_files_from_xml($xml_file, $section)
{
    $xml = simplexml_load_file($xml_file, NULL, LIBXML_NOERROR);

    $result = array();

    if (!empty($xml)) {
        // Get files list
        if (isset($xml->$section)) {
            foreach ($xml->$section->item as $item) {
                $result[] = (string) $item['file'];
            }
        }
    }

    return $result;
}


function fn_uc_update_installed_upgrades($data)
{
    return fn_put_contents(DIR_UPGRADE . 'installed_upgrades.php', "<?php\n if (!defined('BOOTSTRAP')) { die('Access denied'); }\n \$udata = " . var_export($data, true) . ";\n?>");
}

/**
 * Cleanup upgrade cache
 *
 * @param string $package package name
 * @param string $type upgrade type (upgrade/revert)
 * @return boolean always true
 */
function fn_uc_cleanup_cache($package, $type)
{
    if ($type == 'upgrade') {
        @unlink(DIR_UPGRADE . $package . '/backup/uc.sql.tmp');
    } else {
        @unlink(DIR_UPGRADE . $package . '/uc.sql.tmp');
    }
}

/**
 * Check if array item exists in the string
 *
 * @param string $value string to search array item in
 * @param string $array items list
 * @return boolean true if value found, false - otherwise
 */
function fn_uc_check_array_value($value, $array)
{
    foreach ($array as $v) {
        if (strpos($value, $v) !== false) {
            return true;
        }
    }

    return false;
}

function fn_uc_rm($path)
{
    // Try to make directory using php
    $r = fn_uc_is_writable($path, true);

    $result = $r['result'];
    if ($r['method'] == 'fs') {
        $result = fn_rm($path);
    } elseif ($r['method'] == 'ftp') {
        $result = fn_uc_ftp_rm($path);
    }

    return $result;
}

function fn_uc_ftp_rm($path)
{
    $ftp = Registry::get('uc_ftp');
    if (is_resource($ftp)) {
        $rel_path = str_replace(DIR_ROOT . '/', '', $path);
        if (is_file($path)) {
            return @ftp_delete($ftp, $rel_path);
        }

        // Loop through the folder
        if (is_dir($path)) {
            $dir = dir($path);
            while (false !== $entry = $dir->read()) {
                // Skip pointers
                if ($entry == '.' || $entry == '..') {
                    continue;
                }
                if (fn_uc_ftp_rm($path . '/' . $entry) == false) {
                    return false;
                }
            }
            // Clean up
            $dir->close();

            return @ftp_rmdir($ftp, $rel_path);
        } else {
            return false;
        }
    }

    return false;
}


// ------------------------------

class Registry
{
    private static $storage;

    public static function set($k, $v)
    {
        self::$storage[$k] = $v;

        return true;
    }

    public static function get($k)
    {
        return isset(self::$storage[$k])? self::$storage[$k] : NULL;
    }
}

function fn_unified_path($path)
{
    if (defined('IS_WINDOWS')) {
        $path = str_replace('\\', '/', $path);
    }

    return $path;
}

function fn_echo($value)
{
    echo $value;
    fn_flush();
}

function fn_get_file_ext($filename)
{
    $i = strrpos($filename, '.');
    if ($i === false) {
        return '';
    }

    return substr($filename, $i + 1);
}

function fn_parse_queries(&$ret, $sql)
{
    $sql_len      = strlen($sql);
    $char         = '';
    $string_start = '';
    $in_string    = FALSE;
    $time0        = time();

    $i = -1;
    while ($i < $sql_len) {
        $i++;
        if (!isset($sql[$i])) {
            return $sql;
        }
        $char = $sql[$i];


        // We are in a string, check for not escaped end of strings except for
        // backquotes that can't be escaped
        if ($in_string) {
            for (;;) {
                $i         = strpos($sql, $string_start, $i);
                // No end of string found -> add the current substring to the
                // returned array
                if (!$i) {
//                    $ret[] = $sql;
                    return $sql;
                }
                // Backquotes or no backslashes before quotes: it's indeed the
                // end of the string -> exit the loop
                else if ($string_start == '`' || $sql[$i - 1] != '\\') {
                    $string_start      = '';
                    $in_string         = FALSE;
                    break;
                }
                // one or more Backslashes before the presumed end of string...
                else {
                    // ... first checks for escaped backslashes
                    $j                     = 2;
                    $escaped_backslash     = FALSE;
                    while ($i- $j > 0 && $sql[$i - $j] == '\\') {
                        $escaped_backslash = !$escaped_backslash;
                        $j++;
                    }
                    // ... if escaped backslashes: it's really the end of the
                    // string -> exit the loop
                    if ($escaped_backslash) {
                        $string_start  = '';
                        $in_string     = FALSE;
                        break;
                    }
                    // ... else loop
                    else {
                        $i++;
                    }
                } // end if...elseif...else
            } // end for
        } // end if (in string)

        // We are not in a string, first check for delimiter...
        else if ($char == ';') {
            // if delimiter found, add the parsed part to the returned array
            $ret[]      = substr($sql, 0, $i);
            $sql        = ltrim(substr($sql, min($i + 1, $sql_len)));
            $sql_len    = strlen($sql);
            if ($sql_len) {
                $i = -1;
            } else {
                // The submited statement(s) end(s) here
                return '';
            }
        } // end else if (is delimiter)

        // ... then check for start of a string,...
        else if (($char == '"') || ($char == '\'') || ($char == '`')) {
            $in_string    = TRUE;
            $string_start = $char;
        } // end else if (is start of string)

        // ... for start of a comment (and remove this comment if found)...
        else if ($char == '#' || ($i > 1 && $sql[$i - 2] . $sql[$i - 1] == '--')) {
            $sql = substr($sql, strpos($sql, "\n") + 1);
            $sql_len = strlen($sql);
            $i = -1;
        } // end else if (is comment)
    } // end for

    // add any rest to the returned array
    if (!empty($sql) && ereg('[^[:space:]]+', $sql)) {
        return $sql;
    }

    return '';
}

function fn_put_contents($location, $content, $base_dir = '')
{
    $result = '';
    $path = $base_dir . $location;

    if (!empty($base_dir) && !fn_check_path($path)) {
        return false;
    }

    // Location is regular file
    return @fn_file_put_contents($path, $content);
}

function fn_file_put_contents($filename, $data, $flags = 0, $context = null)
{
    $result = ($context === null) ? file_put_contents($filename, $data, $flags) : file_put_contents($filename, $data, $flags, $context);
    if ($result !== false) {
        @chmod($filename, DEFAULT_FILE_PERMISSIONS);
    }

    return $result;
}

function fn_check_path($path)
{
    $real_path = realpath($path);

    return str_replace('\\', '/', $real_path) == $path ? true : false;
}

function db_query($query)
{
    return mysql_query($query);
}

function fn_rm($source, $delete_root = true, $pattern = '')
{
    // Simple copy for a file
    if (is_file($source)) {
        $res = true;
        if (empty($pattern) || (!empty($pattern) && preg_match('/' . $pattern . '/', basename($source)))) {
            $res = @unlink($source);
        }

        return $res;
    }

    // Loop through the folder
    if (is_dir($source)) {
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }
             if (fn_rm($source . '/' . $entry, true, $pattern) == false) {
                return false;
            }
        }
        // Clean up
        $dir->close();

        return ($delete_root == true && empty($pattern)) ? @rmdir($source) : true;
    } else {
        return false;
    }
}

function fn_mkdir($dir, $perms = DEFAULT_DIR_PERMISSIONS)
{
    $result = false;

    // Truncate the full path to related to avoid problems with
    // some buggy hostings
    if (strpos($dir, DIR_ROOT) === 0) {
        $dir = './' . substr($dir, strlen(DIR_ROOT) + 1);
        $old_dir = getcwd();
        chdir(DIR_ROOT);
    }

    if (!empty($dir)) {
        $result = true;
        if (@!is_dir($dir)) {
            $dir = fn_normalize_path($dir, '/');
            $path = '';
            $dir_arr = array();
            if (strstr($dir, '/')) {
                $dir_arr = explode('/', $dir);
            } else {
                $dir_arr[] = $dir;
            }

            foreach ($dir_arr as $k => $v) {
                $path .= (empty($k) ? '' : '/') . $v;
                if (!@is_dir($path)) {
                    umask(0);
                    mkdir($path, $perms);
                }
            }
        }
    }

    if (!empty($old_dir)) {
        chdir($old_dir);
    }

    return $result;
}

function fn_normalize_path($path, $separator = '/')
{

    $result = array();
    $path = preg_replace("/[\\\\\/]+/S", $separator, $path);
    $path_array = explode($separator, $path);
    if (!$path_array[0]) {
        $result[] = '';
    }

    foreach ($path_array as $key => $dir) {
        if ($dir == '..') {
            if (end($result) == '..') {
               $result[] = '..';
            } elseif (!array_pop($result)) {
               $result[] = '..';
            }
        } elseif ($dir != '' && $dir != '.') {
            $result[] = $dir;
        }
    }

    if (!end($path_array)) {
        $result[] = '';
    }

    return fn_is_empty($result) ? '' : implode($separator, $result);
}

function fn_is_empty($var)
{
    if (!is_array($var)) {
        return (empty($var));
    } else {
        foreach ($var as $k => $v) {
            if (empty($v)) {
                unset($var[$k]);
                continue;
            }

            if (is_array($v) && fn_is_empty($v)) {
                unset($var[$k]);
            }
        }

        return (empty($var)) ? true : false;
    }
}

function fn_get_contents($location, $base_dir = '')
{
    $result = '';
    $path = $base_dir . $location;

    if (!empty($base_dir) && !fn_check_path($path)) {
        return $result;
    }

    // Location is regular file
    return @file_get_contents($path);
}

function fn_get_dir_contents($dir, $get_dirs = true, $get_files = false, $extension = '', $prefix = '')
{

    $contents = array();
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {

            // $extention - can be string or array. Transform to array.
            $extension = is_array($extension) ? $extension : array($extension);

            while (($file = readdir($dh)) !== false) {
                if ($file == '.' || $file == '..' || $file{0} == '.') {
                    continue;
                }

                if ((is_dir($dir . '/' . $file) && $get_dirs == true) || (is_file($dir . '/' . $file) && $get_files == true)) {
                    if ($get_files == true && !fn_is_empty($extension)) {
                        // Check all extentions for file
                        foreach ($extension as $_ext) {
                             if (substr($file, -strlen($_ext)) == $_ext) {
                                $contents[] = $prefix . $file;
                                break;
                             }
                        }
                    } else {
                        $contents[] = $prefix . $file;
                    }
                }
            }
            closedir($dh);
        }
    }

    asort($contents, SORT_STRING);

    return $contents;
}

function db_connect($db_host, $db_user, $db_password, $db_name)
{
    if ($db = @mysql_connect($db_host, $db_user, $db_password)) {
        if (@mysql_select_db($db_name, $db)) {
            mysql_query("SET NAMES 'utf8'");

            return true;
        } else {
            return fn_tr('error_select_db');
        }
    } else {
        return fn_tr('error_connect_db');
    }
}

function fn_tr($l)
{
    static $lang = array(
        'text_uc_ftp_cart_directory_not_found' => 'Directory with CS-Cart was not found on your server',
        'text_uc_ftp_login_failed' => 'FTP log in failed',
        'text_uc_ftp_connect_failed' => 'FTP connection failed',
        'text_uc_no_ftp_module' => 'No FTP module was found',
        'error_select_db' => 'Database selection failed',
        'error_connect_db' => 'Database connection failed',
        'text_connecting_to_ftp' => '<br /><br /><b>Connecting to FTP...</b>&nbsp;',
        'ok' => 'OK',
        'text_copying_files' => '<br /><br /><b>Restoring files...</b>&nbsp;',
        'text_removing_obsolete_files' => '<br /><br /><b>Removing obsolete files...</b>&nbsp;',
        'text_updating_database' => '<br /><br /><b>Updating database...</b>&nbsp;',
        'done' => '<br /><br /><b>Done: your store was restored successfully</b>&nbsp;',
        'text_uc_unable_to_remove_upgrade_lock' => 'Failed to remove the upgrade lock file. Please remove the [file] file.',
    );

    return $lang[$l];
}

function fn_flush()
{
    if (function_exists('ob_flush')) {
        @ob_flush();
    }

    flush();
}

function fn_print_r()
{
    static $count = 0;
    $args = func_get_args();

    if (!empty($args)) {
        echo '<ol style="font-family: Courier; font-size: 12px; border: 1px solid #dedede; background-color: #efefef; float: left; padding-right: 20px;">';
        foreach ($args as $k => $v) {
            $v = htmlspecialchars(print_r($v, true));
            if ($v == '') {
                $v = '    ';
        }

            echo '<li><pre>' . $v . "\n" . '</pre></li>';
        }
        echo '</ol><div style="clear:left;"></div>';
    }
    $count++;
}

function fn_print_die()
{
    $args = func_get_args();
    call_user_func_array('fn_print_r', $args);
    die();
}
