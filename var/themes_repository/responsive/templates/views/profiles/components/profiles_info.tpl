{include file="common/subheader.tpl" title=__("customer_information")}

{assign var="profile_fields" value=$location|fn_get_profile_fields}
{split data=$profile_fields.C size=2 assign="contact_fields" simple=true size_is_horizontal=true}

<table class="ty-profiles-info">
    <tr class="ty-valign-top">
        {if $profile_fields.B}
            <td id="tygh_order_billing_adress" class="ty-profiles-info__billing" style="width: 31%">
                <h5 class="ty-profiles-info__title">{__("billing_address")}</h5>
                <div class="ty-profiles-info__field">{include file="views/profiles/components/profile_fields_info.tpl" fields=$profile_fields.B title=__("billing_address")}</div>
            </td>
        {/if}
        {if $profile_fields.S}
            <td id="tygh_order_shipping_adress" class="ty-profiles-info__shipping" style="width: 31%">
                <h5 class="ty-profiles-info__title">{__("shipping_address")}</h5>
                <div class="ty-profiles-info__field">{include file="views/profiles/components/profile_fields_info.tpl" fields=$profile_fields.S title=__("shipping_address")}</div>
            </td>
        {/if}
        <td style="width: 35%">
            {if $contact_fields.0}
                {capture name="contact_information"}
                    {include file="views/profiles/components/profile_fields_info.tpl" fields=$contact_fields.0 title=__("contact_information")}
                {/capture}
                {if $smarty.capture.contact_information|trim != ""}
                    <h5 class="ty-profiles-info__title">{__("contact_information")}</h5>
                    <div class="ty-profiles-info__field">{$smarty.capture.contact_information nofilter}</div>
                {/if}
            {/if}
        </td>
    </tr>
</table>