<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


    $addon_settings = Registry::get('addons.my_changes');
    $secret_key = $addon_settings['secret_key'];
    $secret_key_for_token = $addon_settings['secret_key_for_token'];
    $redirect_url_ok = $addon_settings['ok_url'];
    $redirect_url_fail = $addon_settings['fail_url'];

if ($mode == 'get_stachka_token_simple') {

    $params = $_REQUEST;




    if(empty($params['email'])) {
        fn_echo('empty email');
        exit;
    } elseif (empty($params['first_name'])) {
        fn_echo('empty first name');
        exit;
    } elseif (empty($params['last_name'])) {
        fn_echo('empty last name');
        exit;
    } elseif (empty($params['stachka_id'])) {
        fn_echo('empty stachka id');
        exit;
    }
    

    $condition = db_quote('stachka_id = ?s', $params['stachka_id']);

    $user_data = db_get_row("SELECT user_id, password FROM ?:users WHERE $condition");

    if (empty($user_data['user_id'])) {
        Registry::get('settings.General.address_position') == 'billing_first' ? $address_zone = 'b' : $address_zone = 's';
        $user_data = array();
        $user_data['stachka_id'] = $params['stachka_id'];
        $user_data['email'] = $params['email'] ;
        $user_data['user_login'] = $params['email'];
        $user_data['user_type'] = 'C';
        $user_data['is_root'] = 'N';
        $user_data['password1'] = $user_data['password2'] = md5($data['first_name'].$data['last_name'].$params['email']);
        $user_data[$address_zone . '_firstname'] = $params['first_name'];
        $user_data[$address_zone . '_lastname'] = $params['last_name'];

        if (empty($user_data['email'])) {
            $user_data['email'] = 'noemail-' . TIME . '@example.com';
        }

        list($user_data['user_id'], $profile_id) = fn_update_user('', $user_data, $auth, true, false, false);
    }

    fn_user_logout($auth);

    $token = md5(TIME . $params['stachka_id'] . $secret_key_for_token);

    if (!empty($token)) {
        $save_token = array(
           'stachka_token' => $token
        );

        $a = db_query("UPDATE ?:users SET ?u WHERE user_id = ?i", $save_token, $user_data['user_id']);

        fn_echo($token);
    } else {
        fn_echo('error token');
    }

    exit;

}

if ($mode == 'get_stachka_token_test') {

    $condition = db_quote('stachka_id = ?i', $_REQUEST['stachka_id']);

    $user_data = db_get_row("SELECT user_id, password, email, stachka_id FROM ?:users WHERE $condition");

    if (empty($user_data['user_id'])) {

	  fn_print_r(111);
    }

     $secret_key_for_token = '1234';

    $token = md5(TIME . $data['stachka_id'] . $secret_key_for_token);

    if (!empty($token) && !empty($user_data['user_id'])) {
        $save_token = array(
           'stachka_token' => $token,
	   'stachka_id' => $_REQUEST['stachka_id'],
        );
    }
    db_query("UPDATE ?:users SET ?u WHERE user_id = ?i", $save_token, $user_data['user_id']);

    $condition = db_quote('stachka_token = ?s', $save_token['stachka_token']);

    $user_data = db_get_row("SELECT user_id, password FROM ?:users WHERE $condition");

fn_print_die($user_data);

}



if ($mode == 'get_stachka_token') {

    $params = $_REQUEST;

    if(empty($params['email'])) {
        fn_st_response(400, 'empty email');
    } elseif (empty($params['first_name'])) {
        fn_st_response(400, 'empty first name');
    } elseif (empty($params['last_name'])) {
        fn_st_response(400, 'empty last name');
    } elseif (empty($params['key'])) {
        fn_st_response(400, 'empty key');
    } elseif (empty($params['stachka_id'])) {
        fn_st_response(400, 'empty stachka id');
    }

    $data = array();
    $data['first_name'] = $params['first_name'];
    $data['last_name'] = $params['last_name'];
    $data['stachka_id'] = $params['stachka_id'];
    $data['email'] = $params['email'];

    $check_str = implode('|', $data) . '|' .  $secret_key;
    $check_md5 = md5($check_str);

    if($check_md5 != $params['key']) {
        fn_st_response(400, 'error_md5');
    }

    $condition = db_quote('stachka_id = ?i', $data['stachka_id']);

    $user_data = db_get_row("SELECT user_id, password FROM ?:users WHERE $condition");

    if (empty($user_data['user_id'])) {
        Registry::get('settings.General.address_position') == 'billing_first' ? $address_zone = 'b' : $address_zone = 's';
        $user_data = array();
        $user_data['stachka_id'] = $data['stachka_id'];
        $user_data['email'] = $data['email'] ;
        $user_data['user_login'] = $data['email'];
        $user_data['user_type'] = 'C';
        $user_data['is_root'] = 'N';
        $user_data['password1'] = $user_data['password2'] = md5($data['first_name'].$data['last_name'].$data['email']);
        $user_data['s_firstname'] = $data['first_name'];
        $user_data['s_lastname'] = $data['last_name'];
        $user_data['b_firstname'] = $data['first_name'];
        $user_data['b_lastname'] = $data['last_name'];

        if (empty($user_data['email'])) {
            $user_data['email'] = 'noemail-' . TIME . '@example.com';
        }

        list($user_data['user_id'], $profile_id) = fn_update_user('', $user_data, $auth, true, false, false);
    }

    //fn_user_logout($auth);

    $secret_key_for_token = '1234';

    $token = md5(TIME . $data['stachka_id'] . $secret_key_for_token);

    if (!empty($token) && !empty($user_data['user_id'])) {
        $save_token = array(
           'stachka_token' => $token,
	   'stachka_id' => $data['stachka_id'],
        );

        db_query("UPDATE ?:users SET ?u WHERE user_id = ?i", $save_token, $user_data['user_id']);

        fn_st_response(200, $token);
    } else {
        fn_st_response(400, 'error token');
    }

}


if ($mode == 'login_stachka') {

    $params = $_REQUEST;

    if(empty($params['token'])) {
	 // fn_st_response(400, 'empty token');
        fn_redirect($redirect_url_fail, true);
    }

    $condition = db_quote('stachka_token = ?s', $params['token']);

    $user_data = db_get_row("SELECT user_id, password FROM ?:users WHERE $condition");

    if (empty($user_data['user_id'])) {
	//fn_st_response(400, 'empty user_id');
	fn_redirect($redirect_url_fail, true);
    }

    $user_status = (empty($user_data['user_id'])) ? LOGIN_STATUS_USER_NOT_FOUND : fn_login_user($user_data['user_id']);

    $clear_token = array(
       'stachka_token' => ''
    );

    db_query("UPDATE ?:users SET ?u WHERE user_id = ?i", $clear_token, $user_data['user_id']);

    if ($user_status == LOGIN_STATUS_OK) {
		//fn_st_response(200, 'GOOD');
        fn_redirect($redirect_url_ok, true);

    } elseif ($user_status == LOGIN_STATUS_USER_DISABLED) {
		//	fn_st_response(400, 'USER_DISABLED');
        fn_redirect($redirect_url_fail, true);

    } elseif ($user_status == LOGIN_STATUS_USER_NOT_FOUND) {
	  //fn_st_response(400, 'LOGIN_STATUS_USER_NOT_FOUND');
        fn_redirect($redirect_url_fail, true);
    }
   // fn_st_response(200, 'GOOD');
    fn_redirect($redirect_url_ok, true);

    exit;
} 



function fn_st_response($status, $message) {
        $response = array(
            'status' => $status,
            'data' => $message,          
        );

        fn_echo(json_encode($response));
        exit;
}