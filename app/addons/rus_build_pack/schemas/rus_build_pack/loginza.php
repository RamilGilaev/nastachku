<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

$schema = array(
    'google' => __('loginza_google'),
    'yandex' => __('loginza_yandex'),
    'mailruapi' => __('loginza_mailruapi'),
    'mailru' => __('loginza_mailru'),
    'vkontakte' => __('loginza_vkontakte'),
    'facebook' => __('loginza_facebook'),
    'odnoklassniki' => __('loginza_odnoklassniki'),
    'livejournal' => __('loginza_livejournal'),
    'twitter' => __('loginza_twitter'),
    'linkedin' => __('loginza_linkedin'),
    'loginza' => __('loginza_loginza'),
    'myopenid' => __('loginza_myopenid'),
    'webmoney' => __('loginza_webmoney'),
    'rambler' => __('loginza_rambler'),
    'flickr' => __('loginza_flickr'),
    'lastfm' => __('loginza_lastfm'),
    'verisign' => __('loginza_verisign'),
    'aol' => __('loginza_aol'),
    'steam' => __('loginza_steam'),
    'openid' => __('loginza_openid'),
);

return $schema;