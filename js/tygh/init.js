/*
Usage:

<div class="tygh" id="tygh_container">
    <script type="text/javascript" src="http://host/path/widget/init.js?layout=N&url=http://example.com"></script>
</div>

params:
    url: store URL, required
    layout: layout ID, optional
*/

var TYGH_LOADER = {};

(function(S, FACEBOOK) {

    var init_container = 'tygh_container';
    var main_container = 'tygh_main_container';
    var body_container = 'tygh_body_container';
    var host;
    var store_host;
    var store_url;

    S.otherjQ = 'jQuery' in window;
    S.isDone = false;

    S.loadCss = function(css)
    {
        css = typeof(css) == 'Array' ? css : [css];
        var head = document.getElementsByTagName("head")[0];
        var link;

        for (var i = 0; i < css.length; i++) {
            link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = css[i];
            link.media = 'screen';
            head.appendChild(link);
        }
    };

    S.loadScript = function(url, callback)
    {
        callback = callback || null;
        var script = document.createElement("script");
        script.type = "text/javascript";

        if (script.readyState){  //IE
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    if (callback) {
                        callback();
                    }
                }
            };
        } else {  //Others
            script.onload = function(){
                if (callback) {
                    callback();
                }
            };
        }

        script.src = url;

        document.getElementsByTagName("head")[0].appendChild(script);
    };
    
    S.callback = function(data)
    {
        data = data || {};

        S.isDone = true;
        if (data && data.html) {
            response = data;
        } else {
            var ldr = document.getElementById('tygh_ldr'), parent = ldr.parentNode;
            parent.removeChild(ldr);
            var messages = [];
            if (data.error) {
                messages.push(data.error);
            } else if (data.notifications) {
                for (var idx in data.notifications) {
                    messages.push(data.notifications[idx].message);
                }
            } else {
                messages.push('Invalid data received');
            }

            var newdiv = document.createElement('div');
            newdiv.innerHTML = '<p style="text-align: center; font: normal 13px Arial, Helvetica, sans-serif">' + messages.join('<br/>') + '</p>';
            parent.appendChild(newdiv);
        }
    };

    S.done = function()
    {
        if (S.otherjQ) {
            $.noConflict(true);
        }

        if (!Tygh.$.support.cors) {
            S.loadScript(store_host + '/js/lib/postmessage/jquery.ba-postmessage.js');
        } 

        if (FACEBOOK) {
            S.loadScript(FACEBOOK.external);
        }
        
        // override loadCss mehod
        Tygh.$.loadCss = function(css, show_status)
        {
            var head = document.getElementsByTagName("head")[0];
            var style, j, host;
            for (var i = 0; i < css.length; i++) {
                css_url = (css[i].indexOf('://') == -1) ? Tygh.current_location + '/' + css[i] : css[i];
                ajaxGet(css_url, function(data) {

                    data = data.replace(/([^\r\n,{}]+)(,(?=[^}]*{)|\s*{)/gm, function(m) {
                        if (m.indexOf('@media') != -1 || m.indexOf('and (') != -1 || m.indexOf('/*') === 0) {
                            return m;
                        }

                        return 'html#tygh_html body#tygh_body .tygh ' + m;
                    });

                    if (data.indexOf('url(') != -1) {
                        var host = css_url.split('/');
                        host.pop();
                        host = host.join('/') + '/';
                        data = data.replace(/url\((["'])?/gm, 'url($1' + host);
                    }

                    style = document.createElement('style');
                    style.media = 'screen';
                    style.type = 'text/css';
                    style.appendChild(document.createTextNode(data));
                    head.appendChild(style);
                });
            }
        };


        /*
         * In embedded mode this workaround needs to allow 3rd party
         * code append elements to widget container instead of body
         */
        var oAppendTo = Tygh.$.fn.appendTo;
        var oAppend = Tygh.$.fn.append;
        var oPrepend = Tygh.$.fn.prepend;

        Tygh.$.fn.extend({
            appendTo: function(selector, raw)
            {
                if (Tygh.$(selector).is('body') && !raw) {
                    selector = Tygh.$(Tygh.body);
                }
                return oAppendTo.call(this, selector);
            },
            append: function(selector, raw)
            {
                var self = Tygh.$(this);
                if (self.is('body') && !raw) {
                    self = Tygh.$(Tygh.body);
                }
                return oAppend.call(self, selector);
            },
            prepend: function(selector, raw)
            {
                var self = Tygh.$(this);
                if (self.is('body') && !raw) {
                    self = Tygh.$(Tygh.body);
                }
                return oPrepend.call(self, selector);
            }
        });

        Tygh.$.ceAjax('response', response, {
            result_ids: ['tygh_loader'],
            on_ajax_done: function() {
                Tygh.$('#tygh_ldr').remove();
                Tygh.$('#' + init_container).show();
                Tygh.$.runCart('C');
            }
        });

        // This function clears all data, appended to body (dialogs, etc.)
        Tygh.$.ceEvent('on', 'ce.commoninit', function(context) {
            if (context.parent().prop('id') == init_container) {
                Tygh.$('#' + body_container).empty();
                Tygh.$.ceDialog('clear_stack');
            }
        });

        // This function closes opened popups when clicking outside widget container
        Tygh.$(document).on('click', function(e) {
            var jelm = Tygh.$(e.target);

            if (!jelm.closest('.tygh').length) {

                var popups = Tygh.$('.cm-popup-box:visible');
                if (popups.length) {
                    popups.each(function() {
                        var self = Tygh.$(this);
                        if (self.prop('id')) {
                            var sw = Tygh.$('#sw_' + self.prop('id'));
                            if (sw.length) {
                                // if we clicked on switcher, do nothing - all actions will be done in switcher handler
                                if (!jelm.closest(sw).length) {
                                    sw.click();
                                }
                                return;
                            }
                        }

                        self.hide();
                    });
                }
            }
        });

    };

    ready(function() {

        S.body = document.createElement('div');
        S.body.id = body_container;
        S.body.className = 'tygh';
        document.getElementsByTagName('body')[0].appendChild(S.body);

        S.doc = document.querySelectorAll('.tygh');

        var container = document.getElementById(init_container); // thumblr removed id property from script tag, so need to put script inside div
        var s = container.getElementsByTagName('script')[0];
        var url = _parseUrl(s.getAttribute('src'));
        store_host = '//' + url['host'] + url['base_dir'];

        var store_url_data = _parseUrl(url.parsed_query['url']);

        var layout = url.parsed_query['layout'] || '';
        var response = '';

        _setIds();
        s = document.getElementById(init_container);
        s.style.display = 'none';
        
        var newdiv = document.createElement('div');
        newdiv.setAttribute('id', 'tygh_ldr');

        s.parentNode.insertBefore(newdiv, s);

        newdiv.style.height = '100px';

        if (!FACEBOOK && document.doctype === null && navigator.appVersion.indexOf('MSIE 8.') == -1) {
            newdiv.innerHTML = '<p style="text-align: center">Fatal error: No DOCTYPE specified!</p>';
            return false;
        }

        newdiv.style.backgroundPosition = 'center center';
        newdiv.style.backgroundRepeat = 'no-repeat';
        newdiv.style.backgroundImage = 'url(' + store_host + '/images/spinner.gif)';

        var _hash = FACEBOOK ? '#!/' + FACEBOOK.app_data : window.location.hash;
        var u = _parseHash(_hash) || '';
        if (!u && store_url_data.query) {
            u = attachToUrl(u, store_url_data.query);
        }

        store_url = store_host + '/' + u;
        var _store_url = store_url;
        if (layout) {
            _store_url = attachToUrl(_store_url, 's_layout=' + layout);
        }

        var init_context = window.location.href;
        if (window.location.hash) {
            init_context = init_context.split('#')[0];
        }

        if (FACEBOOK) {
            init_context += (init_context.indexOf('?') != -1 ? '&' : '?') + 'fb_app_id=' + FACEBOOK.app_id + '&fb_page_id=' + FACEBOOK.page_id;
        }

        var query = 'full_render=1&result_ids=' + init_container + '&callback=TYGH_LOADER.callback&init_context=';
        query += escape(init_context);

        S.loadScript(attachToUrl(_store_url, query), function() {
            // Hide spinner if something went wrong (callback did not trigger)
            if (!S.isDone) {
                S.callback({ error: 'Undefined error' });
            }
        });
    });

    function ajax()
    {
        if (window.XDomainRequest){
            return new XDomainRequest();
        } else if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        } else {
            return false;
        }
    }

    function ajaxPost(host, data)
    {
        var a = ajax();
        if (a) {
            a.open("POST", host);
            if ('setRequestHeader' in a) {
                a.setRequestHeader("Content-type", 'text/plain');
            }

            var event = 'onload' in a ? 'onload' : 'onreadystatechange';

            a[event] = function(b,c,d) {
                if ('readyState' in a) {
                    if (a.readyState === 4 && a.status == 200) {
                        eval(a.responseText);
                    }
                } else {
                    eval(a.responseText);
                }
            };

            a.send(data);
        }
    }

    function ajaxGet(host, callback)
    {
        var a = ajax();
        if (a) {
            a.open('GET', host);
            var event = 'onload' in a ? 'onload' : 'onreadystatechange';

            a[event] = function(b,c,d) {
                if ('readyState' in a) {
                    if (a.readyState === 4 && a.status == 200) {
                        callback(a.responseText);
                    }
                } else {
                    callback(a.responseText);
                }
            };

            a.send();

        }
    }

    // increase css selector complexity by setting ID to html and body
    function _setIds()
    {
        document.getElementsByTagName('html')[0].id = 'tygh_html';
        document.getElementsByTagName('body')[0].id = 'tygh_body';
    }

    function _parseUrl(str)
    {
        // + original by: Steven Levithan (http://blog.stevenlevithan.com)
        // + reimplemented by: Brett Zamir

        var  o   = {
            strictMode: false,
            key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
            parser: {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-protocol to catch file:/// (should restrict this)
            }
        };
        
        var m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

        while (i--) {
            uri[o.key[i]] = m[i] || "";
        }

        uri.base_dir = '';
        var s = '';
        if (uri.directory) {
            s = uri.directory.split('/');
            s.pop();
            s.pop();
            s.pop();
            uri.base_dir = s.join('/');
        }

        uri.parsed_query = {};
        if (uri.query) {
            var pairs = uri.query.split('&');
            for (i = 0; i < pairs.length; i++) {
                s = pairs[i].split('=');
                uri.parsed_query[s[0]] = unescape(s[1]);
            }
        }

        return uri;
    }

    function _parseHash(hash)
    {
        if (hash.indexOf('%') !== -1) {
            hash = unescape(hash);
        }

        if (hash.indexOf('#!') != -1) {
            var parts = hash.split('#!/');

            return strReplace(stripLeadingSlash(parts[1]), '&amp;', '&') || '';
        }

        return '';
    }

    function attachToUrl(url, part)
    {
        if (url.indexOf(part) == -1) {
            return (url.indexOf('?') !== -1) ? (url + '&' + part) : (url + '?' + part);
        }

        return url;
    }

    function stripTrailingSlash(str)
    {
        return str.substr(-1) == '/' ? str.substr(0, str.length - 1) : str;
    }

    function stripLeadingSlash(str)
    {
        return str[0] == '/' ? str.substr(1) : str;
    }

    function strReplace(str, src, dst)
    {
        return str.toString().split(src).join(dst);
    }

    // from jQuery library
    function ready(callback)
    {
        // Mozilla, Opera and webkit nightlies currently support this event
        if ( document.addEventListener ) {

            if (document.readyState === "loading") {
                // Use the handy event callback
                document.addEventListener( "DOMContentLoaded", function(){
                    document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
                    callback();
                }, false );
            } else if (document.readyState === "interactive" && navigator.userAgent.indexOf("MSIE") !== -1) {
                // Use the less handy event callback because the 'DOMContentLoaded' will not trigger in IE 10
                document.addEventListener( "readystatechange", function(){
                    if (document.readyState === "complete") {
                        document.removeEventListener( "readystatechange", arguments.callee, false );
                        callback();
                    }
                }, false );
            } else {
                setTimeout(callback);
            }

        // If IE event model is used
        } else if ( document.attachEvent && document.readyState !== "complete") {
            // ensure firing before onload,
            // maybe late but safe also for iframes
            document.attachEvent("onreadystatechange", function(){
                if ( document.readyState === "complete" ) {
                    document.detachEvent( "onreadystatechange", arguments.callee );
                    callback();
                }
            });
        } else {
            setTimeout(callback);
        }
    }


}(TYGH_LOADER, 'TYGH_FACEBOOK' in window ? TYGH_FACEBOOK : null));
