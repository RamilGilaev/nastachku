{foreach from=$queue_orders item=queue key=index}
    {assign var="pay" value=($pay_step==$index+1)}

    <div class="step-container{if $pay}-active{/if}">

        <h2 class="step-title{if $pay}-active{/if}{if $complete && !$pay}-complete{/if} clearfix">
            <span class="float-left">{if $queue.paid}<i class="icon-ok"></i>{else}{$index+1}{/if}</span>

            {if $queue.paid}
            <span class="title">{__("paypal_adaptive_paid")} {include file="common/price.tpl" value=$_total|default:$queue.total}</span>
            {else}
            <span class="title">{__("paypal_adaptive_pay")} {include file="common/price.tpl" value=$_total|default:$queue.total}</span>
            {/if}
        </h2>

        <div id="step_{$index+1}_body" class="step-body{if $pay}-active{/if} {if !$pay}hidden{/if} clearfix">

            <div class="clearfix">
                <div class="checkout-inside-block">
                    {include file="addons/paypal_adaptive/views/paypal_adaptive/items.tpl" queue=$queue}
                </div>
            </div>

            <div class="checkout-buttons">
                {include file="buttons/button.tpl" but_href=$script_proceed but_text=__("paypal_adaptive_pay")}
                {if !$exist_paid}
                    &nbsp;{include file="buttons/button.tpl" but_href=$script_cancel but_text=__("paypal_adaptive_cancel") but_role="text"}
                {/if}
            </div>
        </div>
    </div>
{/foreach}

{capture name="mainbox_title"}<span class="secure-page-title classic-checkout-title">{__("paypal_adaptive_progress_payment_order")}<i class="icon-lock"></i></span>{/capture}
