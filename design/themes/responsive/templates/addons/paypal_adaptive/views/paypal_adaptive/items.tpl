{capture name="cartbox"}

<div class="cm-cart-content cm-cart-content-thumb">
    <div class="cart-items">
        <table class="minicart-table">
        {foreach from=$queue.order_ids item=order_id}

            {assign var="_cart_products" value=$orders_data[$order_id].product_groups[0].products}
            {foreach from=$_cart_products key="key" item="p" name="cart_products"}
                {if !$p.extra.parent}
                    <tr class="minicart-separator">

                        <td style="width: 5%" class="cm-cart-item-thumb">{include file="common/image.tpl" image_width="40" image_height="40" images=$p.main_pair no_ids=true}</td>
                        <td style="width: 94%"><a href="{"products.view?product_id=`$p.product_id`"|fn_url}">{$p.product_id|fn_get_product_name nofilter}</a>
                            <p>
                                <span>{$p.amount}</span><span>&nbsp;x&nbsp;</span>{include file="common/price.tpl" value=$p.display_price span_id="price_`$key`_`$dropdown_id`" class="none"}
                            </p>
                        </td>

                    </tr>
                {/if}
            {/foreach}

        {/foreach}
        </table>
    </div>
</div>

{/capture}
{include file="common/mainbox_cart.tpl" title=__("cart_items") content=$smarty.capture.cartbox}
